    program main
    use nrtype
    use GaussJordan

    implicit none

    real(SP), allocatable, dimension(:,:)    :: A, B
    integer                                  :: i,j,n


     n=3

     allocate(A(n,n), B(n,1))

    A(1,:)=(/1.0, 7.0, 11.0/)
    A(2,:)=(/14.0, 24.0, 19.0/)
    A(3,:)=(/7.0, 8.0, 9.0/)
    B(:,1)=(/3.0, 1.0, 2.5/)


!     B=0; forall(i=1:size(B,1)) B(i,i)=1

!    forall(i=1:size(A,1),j=1:size(A,2)) A(i,j)=i+j

!    B=0.0                          ! Si se trata de la matriz identidad
!    forall(i=1:size(B,2)) B(i,i)=1 ! el algoritmo calcula la inversa de A
    
    

    do i=1,size(A,1)
            print '(3f6.2, a2, 3f6.2)',  A(i,:), ' |',  B(i,:)
    enddo
    
    print *, '|---------------------------|'
    
    call GJSolver(A,B)


    print *, '|---------------------------|'

    do i=1,size(A,1)
        print '(3f6.2, a2, f10.5)', A(i,:), ' |', B(i,:)
    enddo


endprogram
